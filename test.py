import re


def six_ic_number_fill(ic_number):
    pre_fill_list=['00','01','30','31','50','51']

    ic_number_list=set()
#pre_fill==pre_fill_list[1]
#pre_fill[0]==pre_fill_list[1][0]
    for pre_fill in pre_fill_list:
        if len(ic_number)==6:
            new_ic_number=pre_fill+ic_number

        elif len(ic_number)==7:
            new_ic_number = pre_fill[0] + ic_number
        else:
            new_ic_number=ic_number
        ic_number_list.add(new_ic_number)

    return list(ic_number_list)

def user_name_clean_blank(user_name):
    user_name = user_name_clean(user_name)

    user_name_list = user_name.split(" ")
    new_user_name_list = []
    for user_name_one in user_name_list:
        if user_name_one:
            new_user_name_list.append(user_name_one)

    user_name = " ".join(new_user_name_list)
    return user_name
    

def user_name_clean(user_name):

    if not user_name:
        return user_name
    user_name=user_name.upper()
    name = re.split(',|\(2ND|@|2ND CARD', user_name)[0]

    return name
    return user_name

def samle_user_name_norm(user_name):
  
    if not user_name:
        return user_name
    #make the name becomes UPPER case
    user_name = user_name.upper()

    #clean the blank between the name
    user_name = user_name_clean_blank(user_name)

    user_name=USER_NAME_SYN_MAP.get(user_name,user_name)

    user_name=user_name_clean_blank(user_name)

    #take all the items inside the user_name_norm_config.USER_NAME_STR_NORM
    for raw_word,norm_word in USER_NAME_STR_NORM.items():

        raw_word=raw_word.upper()
        norm_word=norm_word.upper()
        #replace raw_word with norm_word
        user_name=user_name.replace(raw_word,norm_word)

    #remove (ALIAS)
    new_user_name_list = user_name.split("(ALIAS)")
    #take the first name
    user_name = new_user_name_list[0]

    #loop from thr user_name_norm_config.ABNORMAL_CHARS
    for char_str in ABNORMAL_CHARS:
        #replace the character into empty space
        user_name = user_name.replace(char_str, " ")

    #split the name by ,
    user_name_list = user_name.split(",")

    new_user_name=user_name_list[0]

    #Finds all substrings where the RE matches, and returns them as a list.
    flag = re.findall("B / O|B/O", new_user_name)

    if flag:
        return ""

    new_user_name_list=re.split("/|@",new_user_name)
    if len(new_user_name_list)==2:
        new_user_name=new_user_name_list[0]

    new_user_name = re.sub("|#", "", new_user_name)
    return new_user_name


def user_name_norm(user_name,word_sord=False):


    new_user_name=samle_user_name_norm(user_name)

    double_list_word=map(lambda x:x.upper(),DOUBLE_LIST)
    title_list_word = map(lambda x: x.upper(), TITLE_LIST)
    nameing_list_word = map(lambda x: x.upper(), NAMEING_LIST)

    other_list_word= map(lambda x: x.upper(), OTHER_CLEAN_LIST)

    common_word_list=[]
    #The extend() method adds all the elements of an iterable (list, tuple, string etc.) to the end of the list.
    common_word_list.extend(double_list_word)
    common_word_list.extend(title_list_word)
    common_word_list.extend(nameing_list_word)
    common_word_list.extend(other_list_word)

    new_patient_name_list=new_user_name.split()
    new_word_list=[]
    for name_word in new_patient_name_list:

        if not name_word or name_word==" ":
            continue

        if name_word not in common_word_list:

            new_word_list.append(name_word)
    if not new_word_list:
        return new_user_name
    if word_sord:
        new_word_list.sort()

    return " ".join(new_word_list)
    
if __name__ == "__main__":

    #print(six_ic_number_fill('1123456'))
    print(user_name_norm('TESTING'))
